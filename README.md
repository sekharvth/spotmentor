# README #

The run.py file contains the code necessary for inference on the dev(test) set. Since nothing particular was specified, I haven't taken the liberty of including code that weould take in an external file as input and churn out predictions.
The code itself splits the data contained the in the data folder into train and test splits (0.2 being the split ratio). 

I've used a Convolutional Neural Network for the task. I also tried out a GRU based architecture and also tried ALBERT (the light version of BERT, that uses just about 18M parameters), but the results weren't as good as the ConvNet, most probably because of sparse data. 
My hypothesis is that the sequential GRU didn't work because the lengths of the input texts were fairly long, and the data was sparse in quantity, so the problems of long term memory retainment combined with lack of data made the GRU model a weak learner.
Regarding the poor performance from ALBERT, I had set the sentence representation vector from ALBERT to be trainable, which would have ended up disturbing its weights too much, while not being able to learn on account of insufficient data.
The results would probably be better if I set the layer to trainable=False.

As for what I would have done differently with more time, I would have loved to dig into the data a bit more. As time wasn't on my side, I could only do a bit of preprocessing, and wasn't able to inspect the data much, which probably would've given me more insight on the architectural nuances I could've used.
Also I would have liked to see if the additional metadata in the json files could be used to further improve the model. I had a pretty packed weekend, so wasn't able to invest too much time into this.

Let me know if you need to know anything furhter, or if you run into any sort of roadblocks.