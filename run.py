import os
import json
import pandas as pd
import re
from sklearn import preprocessing
import keras
import sys
import numpy as np
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Dense, Input, GlobalMaxPooling1D
from keras.layers import Conv1D, MaxPooling1D, Embedding, GRU, Dropout
from keras.models import Model
from keras.initializers import Constant


# get job description in text format, append it to list 'lst', with basic preprocessing
lst = []
print ('Reading files from docs...')
for i in os.listdir('data/docs'):
  with open('data/docs/{}'.format(i), 'rb') as f:
    a = json.load(f)
    if a['jd_information']['description'] == '':
      continue
    text = a['jd_information']['description']
    text = re.sub('&amp;', 'and', text)
    text = re.sub('&nbsp;', ' ', text)
    text = re.sub('\!{2,}', '! ', text)
    text = re.sub('\.{2,}', '. ', text)
    text = re.sub(',', ', ', text)
    lst.append((text, int(re.findall('\d{1,8}', i)[0])))

# merge job description and corresponding dept to single dataframe, call it 'final'
df_ = pd.DataFrame()
text, id = zip(*lst)
df_['text'] = text
df_['Document ID'] = id
df = pd.read_csv('data/document_departments.csv')
final = pd.merge(df, df_, on = 'Document ID')

print ('Final dataframe with jd and correspoding dept created...')

# label-encode depts, as there are 27 classes
le = preprocessing.LabelEncoder()
final['label'] = le.fit_transform(final['Department'])
final = final[['text', 'label']]

print ('Depts converted to label-encoding')

print ('Downloading GloVe vectors. This may take some time...')
os.system('wget http://nlp.stanford.edu/data/glove.6B.zip && unzip glove.6B.zip')
print ('Downloaded GloVe vectors')


BASE_DIR = os.getcwd()
GLOVE_DIR = BASE_DIR
MAX_SEQUENCE_LENGTH = 1000
MAX_NUM_WORDS = 20000
EMBEDDING_DIM = 300
VALIDATION_SPLIT = 0.2

# following commented portion only required for training the model

# print ('Mapping words to embeddings..')

# embeddings_index = {}
# with open(os.path.join(GLOVE_DIR, 'glove.6B.300d.txt')) as f:
#     for line in f:
#         word, coefs = line.split(maxsplit=1)
#         coefs = np.fromstring(coefs, 'f', sep=' ')
#         embeddings_index[word] = coefs


print('Processing the dataset...')

texts = final.text.tolist()  # list of text samples
labels = final.label.tolist()  # list of label ids

print('Found %s data points.' % len(texts))

# vectorize the text samples into a 2D integer tensor
tokenizer = Tokenizer(num_words=MAX_NUM_WORDS)
tokenizer.fit_on_texts(texts)
sequences = tokenizer.texts_to_sequences(texts)

word_index = tokenizer.word_index
print('Found %s unique tokens.' % len(word_index))

# pad sequences to make all examples same length
data = pad_sequences(sequences, maxlen=MAX_SEQUENCE_LENGTH)

# make lables into one hot encoding
labels = to_categorical(np.asarray(labels))
print('Shape of data tensor:', data.shape)
print('Shape of label tensor:', labels.shape)

# split the data into a training set and a validation set
indices = np.arange(data.shape[0])

# set seed to reproduce results and shuffle data
np.random.seed(10)
np.random.shuffle(indices)
data = data[indices]
labels = labels[indices]
num_validation_samples = int(VALIDATION_SPLIT * data.shape[0])

# make train and val sets
x_train = data[:-num_validation_samples]
y_train = labels[:-num_validation_samples]
x_val = data[-num_validation_samples:]
y_val = labels[-num_validation_samples:]


# ---------** training code **-----------
# print('Preparing embedding matrix...')
# num_words = min(MAX_NUM_WORDS, len(word_index) + 1)
# embedding_matrix = np.zeros((num_words, EMBEDDING_DIM))
# for word, i in word_index.items():
#     if i >= MAX_NUM_WORDS:
#         continue
#     embedding_vector = embeddings_index.get(word)
#     if embedding_vector is not None:
#         # words not found in embedding index will be all-zeros.
#         embedding_matrix[i] = embedding_vector

# embedding_layer = Embedding(num_words,
#                             EMBEDDING_DIM,
#                             embeddings_initializer=Constant(embedding_matrix),
#                             input_length=MAX_SEQUENCE_LENGTH,
#                             trainable=False)

# sequence_input = Input(shape=(MAX_SEQUENCE_LENGTH,), dtype='int32')
# embedded_sequences = embedding_layer(sequence_input)
# x = Conv1D(128, 5, activation='relu')(embedded_sequences)
# x = Dropout(0.15)(x)
# x = MaxPooling1D(5)(x)
# x = Conv1D(128, 5, activation='relu')(x)
# x = Dropout(0.15)(x)
# x = MaxPooling1D(5)(x)
# x = Conv1D(128, 5, activation='relu')(x)
# x = GlobalMaxPooling1D()(x)
# x = Dense(128, activation='relu')(x)
# preds = Dense(27, activation='softmax')(x)
# model = Model(sequence_input, preds)
# model.compile(loss='categorical_crossentropy',
#               optimizer='rmsprop',
#               metrics=['accuracy'])
# filepath="weights-{epoch:02d}-{val_acc:.2f}.hdf5"
# checkpoint = keras.callbacks.ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, save_weights_only=False, mode='auto')
# model.fit(x_train, y_train,
#           batch_size=64,
#           epochs=40,
#           validation_data=(x_val, y_val), callbacks = [checkpoint])

# ----------** end training code **-------------

# load best performing model
print ('Loading saved model...')
model = keras.models.load_model('weights-26-0.69.hdf5')
print ('Loaded saved model.')

# get predictions from saved model on the unseen data
test_preds = [np.argmax(i) for i in model.predict(x_val)]
correct = 0

# convert one-hot encoded labels back to integers
y_val = [np.where(r==1)[0][0] for r in y_val]
for pos, i in enumerate(test_preds):
  if i == y_val[pos]:
    correct+=1
print ('test accuracy : ', correct/len(x_val))

# calcualte accuracy on train set
train_preds = [np.argmax(i) for i in model.predict(x_train)]
correct = 0
y_train = [np.where(r==1)[0][0] for r in y_train]
for pos, i in enumerate(train_preds):
  if i == y_train[pos]:
    correct+=1
print ('train accuracy : ', correct/len(x_train))
